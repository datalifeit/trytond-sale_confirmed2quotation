# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.pyson import Eval, If

__all__ = ['Sale']


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls._transitions.add(('confirmed', 'quotation'))
        cls._buttons['quote']['invisible'] &= (Eval('state') != 'confirmed')
        cls._buttons['quote'].update({
            'icon': If(Eval('state') == 'confirmed',
                'tryton-back', 'tryton-forward')
        })
