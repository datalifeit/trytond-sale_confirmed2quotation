# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .sale import Sale


def register():
    Pool.register(
        Sale,
        module='sale_confirmed2quotation', type_='model')
